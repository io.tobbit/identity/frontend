module.exports = {
  publicPath: '',
  pages: {
    index: {
      entry: 'src/pages/index/IndexPageBootstrap.ts',
      template: 'public/bootstrap.html',
      filename: 'index.html',
      title: 'Index Page',
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    },
    login: {
      entry: 'src/pages/login/LoginPageBootstrap.ts',
      template: 'public/bootstrap.html',
      filename: 'login.html',
      title: 'Login Page',
      chunks: ['chunk-vendors', 'chunk-common', 'login']
    },
    register: {
      entry: 'src/pages/signup/SignUpPageBootstrap.ts',
      template: 'public/bootstrap.html',
      filename: 'signup.html',
      title: 'Sign Up Page',
      chunks: ['chunk-vendors', 'chunk-common', 'signup']
    }
  },
  devServer: {
    proxy: 'http://localhost:8081/'
  }
}
