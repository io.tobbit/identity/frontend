import {Component, Prop, Vue, Watch} from 'vue-property-decorator';
import axios, {AxiosResponse} from 'axios';
import {ClientInfo} from "@/models/client-info";

@Component({
  components: {},
})
export default class AppHeader extends Vue {

  @Prop({default: null})
  clientId!: string | null;

  @Watch("clientId", {immediate: true})
  clientIdWatcher(value: string | null, oldValue: string | null) {
    this.updateInfo(value);
  }

  clientInfo: ClientInfo | null = null;

  private updateInfo(value: string | null): void {
    if (value == null) {
      this.clientInfo = null;
    } else {
      axios.get(`/client/${value}`)
        .then((clientInfoResponse: AxiosResponse<ClientInfo>) => {
          this.clientInfo = clientInfoResponse.data;
        }, () => {
          this.clientInfo = null;
        });
    }
  }
}
