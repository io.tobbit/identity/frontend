export interface ClientInfo {
  clientId: string;
  clientName: string;
  imageUrl: string;
}
