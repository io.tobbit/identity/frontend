import Vue from 'vue';
import SignUpPage from './SignUpPage.vue';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(SignUpPage),
}).$mount('#app');
