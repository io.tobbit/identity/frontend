import { Component, Vue } from 'vue-property-decorator';
import HelloWorld from '../../components/HelloWorld.vue';

@Component({
  components: {
    HelloWorld,
  },
})
export default class IndexPage extends Vue {
}
