import {Component, Vue} from 'vue-property-decorator';
import AppHeader from '../../components/app-header/AppHeader.vue';
import AppFooter from '../../components/app-footer/AppFooter.vue';

@Component({
  components: {
    AppHeader,
    AppFooter,
  },
})
export default class LoginPage extends Vue {
  private searchParams = new URL(window.location.href).searchParams;
  clientId: string | null = this.searchParams.get('client_id');
  nextUrl: string | null = this.searchParams.get('next');
}
